<?php

namespace Database\Seeders;

use App\Models\Abogado;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AbogadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Abogado::factory(50)->create();
    }
}
