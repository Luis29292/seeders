<?php

namespace Database\Seeders;

use App\Models\EmpresaAbogado;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmpresaAbogadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        EmpresaAbogado::factory(50)->create();
    }
}
