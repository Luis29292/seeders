<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Empresa>
 */
class EmpresaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $prioridades=\App\Models\Prioridad::all();
        return [
            //
            'nombre'=>$this->faker->company(),
            'descripcion'=>$this->faker->bs(),
            'telefono'=>$this->faker->numerify('55########'),
            'correo'=>$this->faker->safeEmail(),
            'direccion'=>$this->faker->address(),
            'id_prioridad'=>$this->faker->randomElement($prioridades)->id,
        ];
    }
}
