<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EmpresaAbogado>
 */
class EmpresaAbogadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $empresas=\App\Models\Empresa::all();
        $abogados=\App\Models\Abogado::all();
        return [
            'id_abogado'=>$this->faker->randomElement($abogados)->id,
            'id_empresa'=>$this->faker->randomElement($empresas)->id,
        ];
    }
}
