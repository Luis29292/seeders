<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AppModelsAbogado>
 */
class AbogadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //
            'nombre'=>$this->faker->firstName(),
            'apellido'=>$this->faker->lastName(),
            'fecha_registro'=>$this->faker->date($format='Y-m-d',$max='now'),
        ];
    }
}
