-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-02-2024 a las 03:44:05
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empresas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abogados`
--

CREATE TABLE `abogados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `apellido` varchar(250) NOT NULL,
  `fecha_registro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `abogados`
--

INSERT INTO `abogados` (`id`, `nombre`, `apellido`, `fecha_registro`) VALUES
(2, 'Juan', 'Hernández', '2003-12-31'),
(3, 'Rhiannon', 'Ward', '2019-04-24'),
(4, 'Bertha', 'Mills', '1996-01-03'),
(5, 'Rudolph', 'Hodkiewicz', '2022-02-03'),
(6, 'Vinnie', 'Mertz', '1975-04-23'),
(7, 'Gene', 'Feeney', '2018-01-06'),
(8, 'Uriel', 'Marks', '2006-10-19'),
(9, 'Cathrine', 'Ritchie', '1993-02-28'),
(10, 'Sophia', 'Ullrich', '2022-11-25'),
(11, 'Chadd', 'Christiansen', '2023-02-20'),
(12, 'Macy', 'Eichmann', '1992-10-13'),
(13, 'Paula', 'Keeling', '1980-01-15'),
(14, 'Regan', 'Greenfelder', '1987-09-23'),
(15, 'Claire', 'Gerlach', '2021-08-14'),
(16, 'Cade', 'Casper', '1975-08-18'),
(17, 'Alexandra', 'Erdman', '1999-07-21'),
(18, 'Yadira', 'Bailey', '1978-09-20'),
(19, 'Orie', 'Collier', '1973-12-09'),
(20, 'Augustine', 'Wilkinson', '1982-01-06'),
(21, 'Trenton', 'Welch', '1999-04-12'),
(22, 'Gus', 'Schuster', '2018-07-15'),
(23, 'Misty', 'Oberbrunner', '1998-11-14'),
(24, 'Davion', 'Dare', '2001-08-16'),
(25, 'Katharina', 'Cassin', '1987-08-18'),
(26, 'Hulda', 'Huels', '2013-03-10'),
(27, 'Herminia', 'Heathcote', '1990-01-27'),
(28, 'Manuel', 'Hoeger', '1980-06-03'),
(29, 'Lois', 'Stoltenberg', '2021-10-25'),
(30, 'Trace', 'Hamill', '1973-07-29'),
(31, 'Yesenia', 'O\'Connell', '1994-04-28'),
(32, 'Providenci', 'O\'Reilly', '2014-10-20'),
(33, 'Keshawn', 'Ruecker', '1994-11-01'),
(34, 'Ricardo', 'Hand', '1972-04-21'),
(35, 'Vivianne', 'Zieme', '1984-01-20'),
(36, 'Libbie', 'Skiles', '1994-10-11'),
(37, 'Payton', 'Effertz', '1996-04-08'),
(38, 'George', 'Donnelly', '1989-07-09'),
(39, 'Justyn', 'Bashirian', '1990-06-26'),
(40, 'Katlynn', 'Jacobi', '1994-07-08'),
(41, 'Avis', 'Wisozk', '2000-09-22'),
(42, 'Lonny', 'Boyle', '2023-07-30'),
(43, 'Rubie', 'Medhurst', '2009-09-12'),
(44, 'Chelsey', 'Greenfelder', '2019-03-24'),
(45, 'Jaclyn', 'Maggio', '2010-01-16'),
(46, 'Bertram', 'Kirlin', '1996-05-20'),
(47, 'Russel', 'Crona', '2016-08-22'),
(48, 'Felicita', 'Rogahn', '1973-02-10'),
(49, 'Jonatan', 'Rodriguez', '1970-07-29'),
(50, 'Marlen', 'Carroll', '1971-05-09'),
(51, 'Sebastian', 'Wilkinson', '2006-04-17'),
(52, 'Deven', 'Halvorson', '1987-06-16'),
(53, 'Hal', 'Carter', '2011-11-28'),
(54, 'Jazmyne', 'Turcotte', '1971-12-21'),
(55, 'Xzavier', 'Effertz', '2018-09-12'),
(56, 'Grover', 'Huel', '2014-07-28'),
(57, 'Hayden', 'Aufderhar', '1970-12-21'),
(58, 'Makenna', 'Lockman', '2021-01-11'),
(59, 'Brant', 'Roob', '1989-02-11'),
(60, 'Chasity', 'Abbott', '2017-05-16'),
(61, 'Cathy', 'Stark', '1988-04-03'),
(62, 'Fae', 'Rolfson', '1992-10-04'),
(63, 'Glenna', 'Hintz', '2000-02-24'),
(64, 'Tyrel', 'Schiller', '2014-08-02'),
(65, 'Sabryna', 'Howell', '2017-12-07'),
(66, 'Reymundo', 'Kris', '1987-07-21'),
(67, 'Ezra', 'Graham', '1991-07-08'),
(68, 'Rosario', 'Harris', '1981-11-18'),
(69, 'Lambert', 'Hills', '2012-08-13'),
(70, 'Jacynthe', 'Rodriguez', '2022-11-09'),
(71, 'Gregory', 'Spinka', '1970-04-30'),
(72, 'Lulu', 'Torphy', '2021-04-01'),
(73, 'Kayley', 'Schaefer', '2017-05-13'),
(74, 'Enos', 'Hudson', '1975-11-14'),
(75, 'Malachi', 'Lockman', '1993-04-17'),
(76, 'Raymundo', 'Von', '2000-03-11'),
(77, 'Elta', 'Goyette', '1999-08-25'),
(78, 'Houston', 'Pagac', '2015-05-31'),
(79, 'Royal', 'Gislason', '2003-04-12'),
(80, 'Friedrich', 'Gottlieb', '1991-03-18'),
(81, 'Gerard', 'Ruecker', '2018-11-21'),
(82, 'Gilberto', 'Watsica', '2013-09-21'),
(83, 'Louisa', 'Brekke', '1995-05-17'),
(84, 'Jovan', 'Kozey', '1996-07-12'),
(85, 'Dean', 'Ernser', '2023-03-22'),
(86, 'Vanessa', 'Rippin', '2014-09-26'),
(87, 'Milton', 'Sawayn', '2010-06-27'),
(88, 'Alta', 'Schmeler', '1983-05-27'),
(89, 'Greyson', 'Crist', '2000-05-06'),
(90, 'Kallie', 'Collins', '1999-10-01'),
(91, 'Leta', 'Zboncak', '1982-05-08'),
(92, 'Hilbert', 'Lakin', '2006-07-09'),
(93, 'Clovis', 'Zemlak', '2018-01-28'),
(94, 'Maximilian', 'Gerlach', '1998-09-11'),
(95, 'Madelyn', 'White', '2000-03-12'),
(96, 'Fred', 'Conroy', '2017-08-16'),
(97, 'Friedrich', 'Lang', '1976-05-27'),
(98, 'Faye', 'Wiegand', '2004-06-04'),
(99, 'Crystal', 'Hickle', '1997-05-31'),
(100, 'Peggie', 'Corkery', '1987-09-14'),
(101, 'Jennie', 'Kulas', '1997-12-21'),
(102, 'Anjali', 'Larson', '1972-04-24'),
(103, 'Hyman', 'Hegmann', '2007-01-26'),
(104, 'Bernhard', 'O\'Keefe', '2007-08-22'),
(105, 'Mckenzie', 'Welch', '1993-11-19'),
(106, 'Keegan', 'Nikolaus', '1973-04-07'),
(107, 'Dolly', 'Hirthe', '1996-12-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) NOT NULL,
  `id_prioridad` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`, `descripcion`, `telefono`, `correo`, `direccion`, `id_prioridad`) VALUES
(1, 'Schultz and Sons', 'incubate one-to-one partnerships', '5571276916', 'qcormier@example.org', '55173 Cheyanne Track Apt. 523\nSouth Luigi, IL 39612-8166', 3),
(2, 'Wiza, Cummerata and Terry', 'brand visionary infrastructures', '5533399881', 'weldon.block@example.org', '21949 Casimir Shore\nEstrellaview, WI 50930', 5),
(3, 'Nitzsche-Hoeger', 'iterate magnetic e-services', '5565265855', 'marguerite.mitchell@example.net', '6715 Rickey Shoal Suite 463\nFernandoborough, NH 36923', 5),
(4, 'Kessler, O\'Conner and Lesch', 'target plug-and-play partnerships', '5570901689', 'avery53@example.net', '9168 Abagail Estate\nSouth Nigel, NC 80465', 3),
(5, 'Zieme, Nolan and Mayer', 'extend back-end markets', '5550698373', 'jlueilwitz@example.com', '4176 Eda Corner\nHerzogshire, FL 28797-8952', 5),
(6, 'Kemmer, West and O\'Conner', 'extend proactive bandwidth', '5534749079', 'antonia.ferry@example.net', '68058 Rodriguez Brook\nLake Bessiefurt, NE 68726-9087', 1),
(7, 'Jones LLC', 'evolve end-to-end webservices', '5549624680', 'qhalvorson@example.com', '3480 Greenholt Well Suite 983\nVirgilshire, KY 56093', 2),
(8, 'Altenwerth, Jacobs and Mayert', 'extend wireless webservices', '5590685617', 'meagan.hane@example.com', '863 Purdy Rest Suite 026\nNedbury, AK 42069-1869', 5),
(9, 'Schamberger, Bahringer and Denesik', 'scale customized solutions', '5524883690', 'mraz.micah@example.org', '4527 VonRueden Crossroad\nGerholdside, DC 06684', 1),
(10, 'Reynolds PLC', 'synthesize plug-and-play models', '5520204394', 'camilla38@example.org', '3691 Jared Mountain\nDenesikland, KY 39703-8141', 1),
(11, 'Nikolaus LLC', 'synthesize killer applications', '5594291912', 'jacynthe83@example.org', '60195 Weber Square\nSunnyhaven, MD 43251', 5),
(12, 'Hayes-Shields', 'enable impactful infrastructures', '5501211408', 'cleo48@example.net', '699 Kaley Parkways Suite 592\nWest Dedric, NY 28320', 5),
(13, 'Altenwerth, Gaylord and Kub', 'iterate back-end e-commerce', '5555707521', 'kuhn.richard@example.net', '990 Gussie Branch Suite 749\nKlockostad, TN 86527-0614', 2),
(14, 'Boehm-Welch', 'enable mission-critical e-commerce', '5542836509', 'ruby.ritchie@example.com', '9699 Barrows Harbors\nLake Maye, WV 72472', 5),
(15, 'Mohr Group', 'streamline world-class bandwidth', '5538418423', 'jakubowski.americo@example.org', '96833 Rosamond Glen\nSouth Cordia, ND 98854-5544', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_abogado`
--

CREATE TABLE `empresa_abogado` (
  `id_abogado` bigint(20) UNSIGNED NOT NULL,
  `id_empresa` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `empresa_abogado`
--

INSERT INTO `empresa_abogado` (`id_abogado`, `id_empresa`) VALUES
(7, 5),
(2, 2),
(5, 4),
(4, 4),
(5, 2),
(69, 3),
(34, 5),
(87, 2),
(81, 5),
(35, 2),
(79, 3),
(60, 5),
(38, 3),
(35, 2),
(100, 4),
(46, 7),
(83, 11),
(51, 15),
(17, 15),
(69, 6),
(84, 1),
(72, 2),
(6, 10),
(52, 2),
(6, 13),
(76, 10),
(52, 11),
(104, 4),
(100, 7),
(4, 9),
(101, 6),
(92, 1),
(59, 11),
(26, 10),
(93, 11),
(75, 5),
(43, 2),
(84, 15),
(98, 13),
(27, 11),
(42, 1),
(105, 8),
(64, 1),
(2, 5),
(25, 9),
(54, 3),
(37, 7),
(95, 10),
(28, 15),
(30, 12),
(46, 13),
(26, 8),
(62, 2),
(76, 6),
(45, 12),
(49, 15),
(4, 8),
(20, 7),
(22, 10),
(97, 5),
(96, 15),
(26, 14),
(23, 15),
(62, 8),
(40, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridad`
--

CREATE TABLE `prioridad` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `prioridad`
--

INSERT INTO `prioridad` (`id`, `nombre`) VALUES
(1, 'Alta'),
(2, 'Media'),
(3, 'Baja'),
(5, 'Baja');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `abogados`
--
ALTER TABLE `abogados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_prioridad` (`id_prioridad`);

--
-- Indices de la tabla `empresa_abogado`
--
ALTER TABLE `empresa_abogado`
  ADD KEY `id_abogado` (`id_abogado`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `abogados`
--
ALTER TABLE `abogados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`id_prioridad`) REFERENCES `prioridad` (`id`);

--
-- Filtros para la tabla `empresa_abogado`
--
ALTER TABLE `empresa_abogado`
  ADD CONSTRAINT `empresa_abogado_ibfk_1` FOREIGN KEY (`id_abogado`) REFERENCES `abogados` (`id`),
  ADD CONSTRAINT `empresa_abogado_ibfk_2` FOREIGN KEY (`id_empresa`) REFERENCES `empresas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
